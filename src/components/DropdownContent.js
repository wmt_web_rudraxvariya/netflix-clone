import React from 'react'

const dropdownContent = () => (
  <div className='dropdownContainer'>
    <div className='navigation__container--userLogo'>
      <div className='dropdownContent'>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <div className='dropdownContent--user'></div>&nbsp;&nbsp;&nbsp;
          <p className='dropdownContent--user-text'>Webmobtechian</p>
        </div>
        {/* <div>
          <div className='dropdownContent--user dropdownContent--user-2'></div>
          <p className='dropdownContent--user-text'>Tony</p>
        </div> */}
        {/* <div>
          <div className="dropdownContent--user dropdownContent--user-3"></div>
          <p className="dropdownContent--user-text">Luis</p>
        </div> */}
        <p className='dropdownContent-text'>Manage Profiles</p>
        <p className='dropdownContent-text'>Account</p>
        <p className='dropdownContent-text'>Help Center</p>
        <p className='dropdownContent-text'>Sign out of Netflix</p>
      </div>
      {/* <div className='dropdownContent dropdownContent--2'></div> */}
    </div>
  </div>
)

export default dropdownContent
