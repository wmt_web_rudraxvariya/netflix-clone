import React from 'react'

const footer = () => (
  <footer className='footer'>
    <div className='footer__copyright'>
      &copy; {new Date().getFullYear()}
      {'  '}
      <a className='footer__copyright--link' href='https://webmobtech.com'>
        WebMob Technologies.
      </a>
      {'  '}
      All Rights Reserved.
    </div>
  </footer>
)

export default footer
